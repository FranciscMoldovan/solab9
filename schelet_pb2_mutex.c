#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFF_SIZE 5

int buff[BUFF_SIZE];
int numExisting = 0;
int buff_start = 0, buff_end = 0;

pthread_mutex_t mutex      = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t can_produce = PTHREAD_COND_INITIALIZER;
pthread_cond_t can_consume = PTHREAD_COND_INITIALIZER;

void *prod_func(void *arg)
{
    for(;;)
    {
        sleep(rand()%1);
       
        pthread_mutex_lock(&mutex);
        while(numExisting == BUFF_SIZE){
            pthread_cond_wait(&can_produce, &mutex);
        }
        

            int producedNumber = rand()%100;
            printf("[%d]>>> PRODUCED: %d POS=%d\n", numExisting, producedNumber, buff_end);
            buff[buff_end] = producedNumber;
            numExisting++;
            if(buff_end < BUFF_SIZE -1)
                buff_end++;
            else
                buff_end=0;
        pthread_cond_signal(&can_consume);
        pthread_mutex_unlock(&mutex);

    }

    return NULL;
}

void *cons_func(void *arg)
{
    for(;;)
    {
        sleep(2+rand()%2);

        pthread_mutex_lock(&mutex);
        while(numExisting == 0){
            pthread_cond_wait(&can_consume, &mutex);
        }
        

        printf("[%d]\t<<<CONSUMED: %d POS=%d\n", numExisting, buff[buff_start], buff_start);
        numExisting--;
        if(buff_start < BUFF_SIZE -1)
            buff_start++;
        else
            buff_start=0;
        pthread_cond_signal(&can_produce);
        pthread_mutex_unlock(&mutex);
    }
    return NULL;
}

int main()
{
    pthread_t th_consumer, th_producer;
    pthread_create(&th_consumer, NULL, prod_func, NULL);
    pthread_create(&th_producer, NULL, cons_func, NULL);

    pthread_join(th_producer, NULL);
    pthread_join(th_consumer, NULL);

return 0;   
}