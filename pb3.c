/*
Implement the protocol for crossing a bridge undergoing repairs. The cars can cross 
the bridge in only one direction at one point. We assume that the bridge can sustain 
maximum N cars. When a car arrives in front of the bridge, the thread associated to 
the car will execute the following function:

void Car(int dir)
{
    EnterBridge(dir);
    CrossBridge(dir);
    ExitBridge(dir);
}

Implement the functions listed above, in order to ensure the safety of the traffic.
*/

#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM_THREADS 10
#define MAX_CARS    1

#define LEFT  1
#define RIGHT 2

int current_dir = LEFT;
int current_cars = 0;
int current_crossing_cars = 0;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_can_cross = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_can_enter = PTHREAD_COND_INITIALIZER;
pthread_mutex_t mutex_cross = PTHREAD_MUTEX_INITIALIZER;

typedef struct thStruct
{
    int dir; 
    int id;
}ThreadDataT;

void EnterBridge(int dir, int id)
{
    usleep(random() % 100);
    pthread_mutex_lock(&mutex);

        while(MAX_CARS == current_cars)
        {   // To many cars on the bridge, wait here . . 
            pthread_cond_wait(&cond_can_enter, &mutex);
        }

        // No more waiting, increase number of cars entering, NOT crossing yet
        current_cars++;

        printf("Car %d ENTERED (>>) bridge, dir=%s, total=%d!\n",\
                id, (LEFT==dir?"left":"right"), current_cars);
        
    pthread_mutex_unlock(&mutex);
}

void CrossBridge(int dir, int id)
{
    usleep(rand() % 100);

    pthread_mutex_lock(&mutex_cross);
        // Wait for the indicated direction and places on bridge
        while(current_dir != dir && current_crossing_cars > 0)
        {
            pthread_cond_wait(&cond_can_cross, &mutex_cross);
        }

        if(current_dir != dir)
        {
            current_dir = dir;
        }

        // No more waiting here, increase num cars crossing, you are also crossing
        current_crossing_cars++;

    pthread_mutex_unlock(&mutex_cross);

        printf("Car %d\tCROSSING (==) bridge, dir=%s, total=%d, total crossing:%d!\n",\
            id, (LEFT==dir?"left":"right"), current_cars, current_crossing_cars);
    
    // Time needed to cross
    usleep(100);
    
    // Just about to exit bridge
    pthread_mutex_lock(&mutex_cross);
    current_crossing_cars--;
    pthread_cond_signal(&cond_can_cross); // Anotherone can come in!
        
    pthread_mutex_unlock(&mutex_cross);
}

void ExitBridge(int dir, int id)
{
    usleep(rand() % 100);
    pthread_mutex_lock(&mutex);

        current_cars--;
            
            printf("Car %d\t\tEXITED (<<) bridge, dir=%s, remaining=%d!\n", id,\
                (LEFT==dir?"left":"right"), current_cars);
            
    pthread_cond_signal(&cond_can_enter); // Not yet crossing, but entering is OK 
    
    pthread_mutex_unlock(&mutex);
}

void *car_func(void *arg)
{
    ThreadDataT *p = (ThreadDataT*)arg;

    usleep(random() % 100);

    EnterBridge(p->dir, p->id);
    CrossBridge(p->dir, p->id);
    ExitBridge(p->dir, p->id);
    
    return NULL;
}
    
int main()
{
    pthread_t   threads[NUM_THREADS];
    ThreadDataT params[NUM_THREADS];
    int i;

    srand(time(NULL));

    for(i=0; i<NUM_THREADS; i++)
    {
        params[i].id = i;
        params[i].dir = (i % 2) + 1;
        pthread_create(&threads[i], NULL, car_func, &params[i]);
    }

    for(i=0; i<NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    pthread_mutex_destroy(&mutex);
    pthread_cond_destroy(&cond_can_cross);

    return 0;
}






















