/*
The dining philosophers problem. Assume that N philosophers wish to eat dinner 
in a room which contains a single table. On the table there are N spaghetti plates 
and N forks. In order to be able to eat, a philosopher needs two forks: his own and
the left neighbor's fork. Each philosopher has a unique identification number and 
is represented by a thread which calls in an infinite loop two functions: 
think(int idPhilosopher) and eat(int idPhilosopher). Write the program which generates 
the threads corresponding to the philosopher and the code of the two functions, 
such as the philosophers can eat dinner without any problems. 
The situations which have to be avoided are: deadlock and starving (when
a philosopher waits to eat for an indefinite period of time).
*/

#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM_THREADS 10

#define FORKS_LOCKED            1
#define FORKS_NOT_LOCKED        0
#define STARVATION_TIMEOUT      5000000     // Nanoseconds
#define RUN_TIME                5
#define NANO                    1000000000

#define PRINT_TRYING
#define PRINT_EATING
#define PRINT_STARVING

pthread_mutex_t furculite[NUM_THREADS];
pthread_mutex_t starve_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t print_mutex = PTHREAD_MUTEX_INITIALIZER;

typedef struct tparam
{
    int id;
}ThreadDataT;

int think(int id, int starving)
{
    int neighbour = (id+1)%NUM_THREADS;
#if defined(PRINT_TRYING)
    pthread_mutex_lock(&print_mutex);
    printf("Philoshopher %d is thinking!\n", id);
    pthread_mutex_unlock(&print_mutex);
#endif
    usleep(random()%1000000);
    if(starving)
    {
        pthread_mutex_lock(&starve_mutex);
        pthread_mutex_lock(&furculite[id]);
        pthread_mutex_lock(&furculite[neighbour]);
        pthread_mutex_unlock(&starve_mutex);
        return FORKS_LOCKED;
    }

    if (0 == pthread_mutex_trylock(&furculite[id]))
    {
        if (0 == pthread_mutex_trylock(&furculite[neighbour]))
        {
            return FORKS_LOCKED;
        }
        else
        {
            pthread_mutex_unlock(&furculite[id]);
        }
    }
    return FORKS_NOT_LOCKED;
}

void eat(int id)
{
    int neighbor = (id+1)%NUM_THREADS;
#if defined(PRINT_EATING)
    pthread_mutex_lock(&print_mutex);
    printf("Philosopher %d is eating!\n", id);
    pthread_mutex_unlock(&print_mutex);
#endif
    usleep(random()%1000000);
    pthread_mutex_unlock(&furculite[neighbor]);
    pthread_mutex_unlock(&furculite[id]);
}

void *philosopher_func(void *arg)
{
    ThreadDataT *p = (ThreadDataT*)arg;
    usleep(random()%1000000);
    int starving;
    time_t t_start, t_stop;
    struct timespec ts_eat;
    unsigned long long t_eat, t_starve;
    t_start = time(NULL);
    t_stop = t_start + RUN_TIME;
    clock_gettime(CLOCK_MONOTONIC, &ts_eat);
    t_eat = (ts_eat.tv_sec*NANO) + ts_eat.tv_nsec;

    for(;;)
    {
        time_t t_now = time(NULL);
        starving = 0;
        if (t_now >= t_stop)
        {
            break;
        }
        clock_gettime(CLOCK_MONOTONIC, &ts_eat);
        t_starve = (ts_eat.tv_sec * NANO) + ts_eat.tv_nsec;
        if(t_starve - t_eat > STARVATION_TIMEOUT)
        {
            starving = 1;   
    #if defined(PRINT_STARVING)
            pthread_mutex_lock(&print_mutex);
            printf("Philosopher %d is starving!\n", p->id);
            pthread_mutex_unlock(&print_mutex);
    #endif
        }
        if (FORKS_LOCKED == think(p->id, starving))
        {
    #if defined(PRINT_EATING)
        pthread_mutex_lock(&print_mutex);
        printf("Philosopher %d will eat!\n", p->id);
        pthread_mutex_unlock(&print_mutex);
    #endif
        eat(p->id);
        clock_gettime(CLOCK_MONOTONIC, &ts_eat);
        t_eat = (ts_eat.tv_sec*NANO)+ts_eat.tv_nsec;
    #if defined(PRINT_EATING)
        pthread_mutex_lock(&print_mutex);
        printf("Philosopher %d done eating!\n", p->id);
        pthread_mutex_unlock(&print_mutex);
    #endif
        }
        else
        {
    #if defined(PRINT_TRYING)
        pthread_mutex_lock(&print_mutex);
        printf("Philosopher %d will not eat!\n", p->id);
        pthread_mutex_unlock(&print_mutex);
    #endif  
        }
    }
    

    return NULL;
}

int main()
{
    pthread_t   threads[NUM_THREADS];
    ThreadDataT params[NUM_THREADS];
    int i;

    srand(time(NULL));

    for(i=0; i<NUM_THREADS; i++)
    {
        params[i].id = i;
        pthread_create(&threads[i], NULL, philosopher_func, &params[i]);
    }

    for(i=0; i<NUM_THREADS; i++)
    {
        pthread_join(threads[i], NULL);
    }

    return 0;
}