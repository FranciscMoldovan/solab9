/*
Implement the producer/consumer problem with a circular buffer, 
using the functions described above. A random number of producer
and consumer threads will be created; each thread will do a random 
number of steps. During the ongoing execution list the number of 
producer and consumer threads which are waiting
*/

#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <pthread.h>

#define MAX_PRODUCERS 20
#define MAX_CONSUMERS 5
#define MAX_STEPS     2
#define MAX_QUEUE     2

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_print = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_full = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_empty = PTHREAD_COND_INITIALIZER;

int work_buffer[MAX_QUEUE * 2];
int queue_start = 0, queue_end = 0;
int stop = 0;

typedef struct thStruct
{
    int id;
}ThreadDataT;

void *producer_func(void* param)
{
    ThreadDataT *ptrThData = (ThreadDataT*)param;
    int i;
    usleep(random()%100);
    for(i=0; i<MAX_STEPS; i++)
    {
        // Produce workload
        pthread_mutex_lock(&mutex);
        while( (queue_end + 1) % MAX_QUEUE == (queue_start / 2) )
        {
            // Work buffer is full
            pthread_cond_wait(&cond_full, &mutex);
        }
        work_buffer[queue_end    ] = random() % 1000;
        work_buffer[queue_end + 1] = random() % 1000;
        pthread_mutex_lock(&mutex_print);
        printf("Producer %d produced (%d, %d)\n", ptrThData->id, work_buffer[queue_end], \
                                        work_buffer[queue_end + 1]);

        pthread_mutex_unlock(&mutex_print);
        queue_end += 2;
        usleep(random() % 100);
        pthread_cond_signal(&cond_empty);
        pthread_mutex_unlock(&mutex);
    }
    return NULL;
}

void *consumer_func(void* param)
{
    ThreadDataT* ptrThData = (ThreadDataT*)param;
    int sum;
    usleep(random() % 100);
    for(;;)
    {
        // Consume workload
        pthread_mutex_lock(&mutex);
        while(queue_start == queue_end)
        {
            // Wait buffer is empty
            if(stop)
            {
                pthread_mutex_unlock(&mutex);
                return NULL;
            }
            pthread_cond_wait(&cond_empty, &mutex);
        }
        sum = work_buffer[queue_start] + work_buffer[queue_start + 1];
        pthread_mutex_lock(&mutex_print);
        printf("Consumer %d computed %d + %d = %d\n", 
        ptrThData->id,
        work_buffer[queue_start    ], 
        work_buffer[queue_start + 1], 
        sum);

        queue_start += 2;
        usleep(random() % 100);
        pthread_cond_signal(&cond_full);
        pthread_mutex_unlock(&mutex_print);
        pthread_mutex_unlock(&mutex);
    }
}

int main()
{
    pthread_t threads_producers[MAX_PRODUCERS];
    pthread_t threads_consumers[MAX_CONSUMERS];
    ThreadDataT params_producers[MAX_PRODUCERS];
    ThreadDataT params_consumers[MAX_CONSUMERS];
    int i;

    for(i=0; i<MAX_PRODUCERS; i++)
    {
        params_producers[i].id = i;
        pthread_create(&threads_producers[i], NULL, producer_func, &params_producers[i]);
    }

    for(i=0; i<MAX_CONSUMERS; i++)
    {
        params_consumers[i].id = i;
        pthread_create(&threads_consumers[i], NULL, consumer_func, &params_consumers[i]);
    }

    for(i=0; i<MAX_PRODUCERS; i++)
    {
        pthread_join(threads_producers[i], NULL);
    }

    pthread_mutex_lock(&mutex_print);
    printf("All producers threads exited!\n");
    pthread_mutex_unlock(&mutex_print);
    pthread_mutex_lock(&mutex);
    stop = 1;
    pthread_cond_broadcast(&cond_empty);
    pthread_mutex_unlock(&mutex);

    for(i=0; i<MAX_CONSUMERS; i++)
    {
        pthread_join(threads_consumers[i], NULL);
    }

}






























