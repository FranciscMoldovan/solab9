#include <stdio.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>

#define NR_THREADS 8

pthread_mutex_t open_file_lock;

int open_once(const char *file_name)
{
    static int fd = -1;
    pthread_mutex_lock(&open_file_lock);
    if (fd == -1)
        fd = open(file_name, O_RDONLY);
    pthread_mutex_unlock(&open_file_lock);
    return fd;
}

void* thread_func(void* _arg)
{
    char* file_name = (char*)_arg;
    int fd = open_once(file_name);
    char c;
    while (read(fd, &c, 1) == 1)
	putchar(c); 
    return NULL;
}

int main(int argc, char **argv)
{
    pthread_t threads[NR_THREADS];

    pthread_mutex_init(&open_file_lock, NULL);

    for (int i = 0; i < NR_THREADS; ++i)
        pthread_create(&threads[i], NULL, thread_func, argv[1]);
    
    for (int i = 0; i < NR_THREADS; ++i)
        pthread_join(threads[i], NULL);

    pthread_mutex_destroy(&open_file_lock);

    return 0;
}
