#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#include <stdlib.h>

int n[] = { 18, 17, 24, 143, 99, 6 };
const int nlen = sizeof(n) / sizeof(int);

void* thread_fun(void* _arg)
{
    static int progress = 0;

    pthread_mutex_t* lock = (pthread_mutex_t*)_arg;

    for(;;)
    {
        if (pthread_mutex_lock(lock) != 0)
        {
            perror("Cannot acquire lock\n");
            exit(1);
        }

        // Zona critica

        int terminate = 0;
        if (progress >= nlen)
        {
            printf("Terminating thread...\n");
            terminate = 1;
        }

        int idx = progress++;

        if (pthread_mutex_unlock(lock) != 0)
        {
            perror("Cannot release lock\n");
            exit(1);
        }

        // Zona comuna

        if (terminate)
        {
            break;
        }

        int nrdiv = 0;
        for (int i = 1; i <= n[idx]; ++i)
        {
            if (n[idx] % i == 0)
            {
                ++nrdiv;
            }
        }
        n[idx] = nrdiv;
    }

    return NULL;
}

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Invalid arguments\n");
        return 1;
    }

    int nrthreads;
    if (sscanf(argv[1], "%d", &nrthreads) != 1)
    {
        printf("Invalid arguments\n");
        return 1;
    }

    pthread_t* threads = (pthread_t*)malloc(sizeof(pthread_t) * nrthreads);
    pthread_mutex_t lock;

    if (pthread_mutex_init(&lock, NULL) != 0)
    {
        perror("Cannot init mutex\n");
        return 1;
    }

    for (int i = 0; i < nrthreads; ++i)
    {
        if (pthread_create(&threads[i], NULL, thread_fun, &lock) != 0)
        {
            perror("Cannot init thread\n");
            return 1;
        }
    }

    for (int i = 0; i < nrthreads; ++i)
    {
        if (pthread_join(threads[i], NULL) != 0)
        {
            perror("Cannot join thread\n");
            return 1;
        }
    }

    for (int i = 0; i < nlen; ++i)
    {
        printf("%d ", n[i]);
    }
    putchar('\n');

    free(threads);
    if (pthread_mutex_destroy(&lock) != 0)
    {
        perror("Cannot uninit mutex\n");
        return 1;
    }

    return 0;
}
