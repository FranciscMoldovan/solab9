/*
We assume that at a hairdresser's there exists only a hairdresser and N waiting chairs. 
A single client can be hair-cut at one moment and only maximum N clients can wait inside; 
the other clients have to wait outside, in the street. The hairdresser and the clients 
are represented by using threads. Write the functions executed by these two types of 
threads such as to avoid breaking any of the listed rules. The hairdresser should not 
stay idle if there are clients waiting to be served. We have to avoid the case in which 
a client waiting outside the hairdresser's is served before a client waiting inside 
the hairdresser's. The arrival order of the clients can also be taken into consideration.
*/

#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define NUM_CLIENTS      2
#define NUM_CHAIRS_LOBBY 2

typedef struct client_param
{
    int id;
}ClientParamT;

int lobby_clients = 0;
int clients_outside = 0;
int closed = 0;

pthread_mutex_t mutex_print = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  cond_client_can_enter = PTHREAD_COND_INITIALIZER;

pthread_mutex_t mutex_outside = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_inside = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  cond_space_in_lobby = PTHREAD_COND_INITIALIZER;

void *frizer_func(void *arg)
{
    for(;;)
    {
        pthread_mutex_lock(&mutex_inside);
        while(0 == lobby_clients)
        {
            if(closed)
            {
                pthread_mutex_unlock(&mutex_inside);
                printf("Hair salon C-L-O-S-I-N-G\n");
                return NULL;
            }
            pthread_cond_wait(&cond_client_can_enter, &mutex_inside);
        }

        pthread_mutex_lock(&mutex_print);
        printf("Hairdresser: taking NEW Client!\n");
        pthread_mutex_unlock(&mutex_print);

        lobby_clients--;

        pthread_cond_signal(&cond_space_in_lobby); // Anotherone can come!
        pthread_mutex_unlock(&mutex_inside);
        // Client taken, cut...
        sleep(1+random()%2);
    }
}

void *client_func(void *arg)
{
    ClientParamT *p = (ClientParamT*)arg;
    pthread_mutex_lock(&mutex_outside);
    clients_outside++;

    pthread_mutex_lock(&mutex_print);
    printf("Client %d waiting outside! Total outside=%d\n", p->id, clients_outside);
    pthread_mutex_unlock(&mutex_print);
    
    pthread_mutex_unlock(&mutex_outside);

    // Client wants inside
    pthread_mutex_lock(&mutex_inside);
    // If lobby is full, stay outside .. . 
    while(NUM_CHAIRS_LOBBY == lobby_clients)
    {
        pthread_cond_wait(&cond_space_in_lobby, &mutex_inside);
    }

    // Now some space in lobby
    pthread_mutex_lock(&mutex_outside);
    clients_outside--;
    lobby_clients++;
    
    pthread_mutex_lock(&mutex_print);
    printf("Client %d waiting inside! Total inside=%d!\n", p->id, lobby_clients);
    pthread_mutex_unlock(&mutex_print);
    
    pthread_cond_signal(&cond_client_can_enter); // Someone can come inside the lobby
    pthread_mutex_unlock(&mutex_outside);
    pthread_mutex_unlock(&mutex_inside);

    return NULL;
}

int main()
{
    pthread_t    client_threads[NUM_CLIENTS];
    ClientParamT params[NUM_CLIENTS];
    pthread_t    frizer_thread;
    int i;

    srand(time(NULL));

    pthread_create(&frizer_thread, NULL, frizer_func, NULL);

    for(i=0; i<NUM_CLIENTS; i++)
    {
        params[i].id = i;
        pthread_create(&client_threads[i], NULL, client_func, &params[i]);
    }

    for(i=0; i<NUM_CLIENTS; i++)
    {
        pthread_join(client_threads[i], NULL);
    }


    // Shop owner opening
    pthread_mutex_lock(&mutex_inside); // no-one inside initially!
    closed = 1; // Shop not open yet
    pthread_cond_signal(&cond_client_can_enter
);
    pthread_mutex_unlock(&mutex_inside);

    pthread_join(frizer_thread, NULL);
    return 0;
}

