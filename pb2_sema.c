#include <pthread.h>
#include <stdio.h>
#include <semaphore.h>
#include <unistd.h>
#include <stdlib.h>

#define BUFF_SIZE 5

pthread_t producer_th, consumer_th1, consumer_th2;
sem_t sema_only_one, sema_can_consume, sema_can_produce, sema_print;
int buff[BUFF_SIZE];
int numExisting = 0;
int buff_start = 0, buff_end = 0;

void *producer_func(void *arg)
{
    for(;;)
    {
        sleep(rand()%1);

        int producedNumber = rand()%100;
       
        sem_wait(&sema_can_produce); // Increment the full_of_data indicator
            sem_wait(&sema_only_one); // Check for exclusivity
                buff[buff_end] = producedNumber;
            

            // sem_wait(&sema_print);
            printf("[%d]>>> PRODUCED: %d POS=%d\n", numExisting, producedNumber, buff_end);
            // sem_post(&sema_print);

            numExisting++;
            if(buff_end < BUFF_SIZE -1)
                buff_end++;
            else
                buff_end=0;

            sem_post(&sema_only_one);
        sem_post(&sema_can_consume);
    }
    return NULL;
}

void *consumer_func(void *arg)
{
    for(;;)
    {
        sleep(2+rand()%2);
        sem_wait(&sema_can_consume); // Wait here, as there's nothing to read
            sem_wait(&sema_only_one); // Get exclusive access
            // sem_wait(&sema_print);
                printf("[%d]\t<<<CONSUMED: %d POS=%d\n", numExisting, buff[buff_start], buff_start);
            // sem_post(&sema_print);
            
            numExisting--;
            if(buff_start < BUFF_SIZE -1)
                buff_start++;
            else
                buff_start=0;

            sem_post(&sema_only_one); 
        sem_post(&sema_can_produce);
    }
    return NULL;
}


int main()
{   
    sem_init(&sema_only_one,     0,         1); // ONLY one allowed in
    sem_init(&sema_can_consume,      0,         0); // Initially NO DATA
    sem_init(&sema_can_produce, 0, BUFF_SIZE); // Initially ALL EMPTY
    sem_init(&sema_print,        0,         1);

    pthread_create(&producer_th , NULL, producer_func, NULL);
    pthread_create(&consumer_th1, NULL, consumer_func, NULL);
    pthread_create(&consumer_th2, NULL, consumer_func, NULL);

    pthread_join(consumer_th1, NULL);
    pthread_join(consumer_th2, NULL);
    pthread_join(producer_th, NULL);
return 0;
}
