#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>

#define BUFF_SIZE 5

int buff[BUFF_SIZE];
int numExisting = 0;
int buff_start = 0, buff_end = 0;

sem_t sem_can_produce;
sem_t sem_can_consume;
sem_t sem_only_one;

void *prod_func(void *arg)
{
    for(;;)
    {
        sleep(rand()%1);
        sem_wait(&sem_can_produce);
            sem_wait(&sem_only_one);
                int producedNumber = rand()%100;
                printf("[%d]>>> PRODUCED: %d POS=%d\n", numExisting, producedNumber, buff_end);
                buff[buff_end] = producedNumber;
                numExisting++;
                if(buff_end < BUFF_SIZE -1)
                    buff_end++;
                else
                    buff_end=0;
            sem_post(&sem_only_one);
        sem_post(&sem_can_consume);
    }

    return NULL;
}

void *cons_func(void *arg)
{
    for(;;)
    {
        sleep(2+rand()%2);
        sem_wait(&sem_can_consume);
            sem_wait(&sem_only_one);
                printf("[%d]\t<<<CONSUMED: %d POS=%d\n", numExisting, buff[buff_start], buff_start);
                numExisting--;
                if(buff_start < BUFF_SIZE -1)
                    buff_start++;
                else
                    buff_start=0;
            sem_post(&sem_only_one);
        sem_post(&sem_can_produce);
    }
    return NULL;
}

int main()
{
    pthread_t th_consumer, th_producer;
    pthread_create(&th_consumer, NULL, prod_func, NULL);
    pthread_create(&th_producer, NULL, cons_func, NULL);

    sem_init(&sem_can_consume, 0, 0        );
    sem_init(&sem_can_produce, 0, BUFF_SIZE);
    sem_init(&sem_only_one,    0, 1        );

    pthread_join(th_producer, NULL);
    pthread_join(th_consumer, NULL);

return 0;   
}